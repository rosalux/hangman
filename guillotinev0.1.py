#!/usr/bin/python3
import random
def main():
    for i in range(40):
        print()
    score = 0
    scoreFile = open('highScores')
    scoreArr = scoreFile.read().split("\n")
    scoreFile.close()
    for i in range(len(scoreArr)):
        scoreArr[i] = scoreArr[i].split(' ')
    print('Welcome to Guillotine! The People\'s hangman'.center(100))
    print("\n\n\n\n")
    print('High Scores'.center(100))
    print()
    for i in range 3:
        print((str(i)+':  '+scoreArr[i][0]+'  -  ' + scoreArr[i][1]).center(100))
        print("\n")
    print("\n\n\n\n")
    nada = input('Press ENTER when ready Comrade\n\n\n\n\n\n\n'.center(100))
    print("\n\n\n\n\n\n\n\n\n\n\n")
    
    while True:
        wrong = 0
        word = getWord()
        wordRev = []
        for a in word:
            if a == ' ':
                wordRev.append('  ')
            else:
                wordRev.append('_')
        used = []
        rem = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']
        game(word,wordRev,wrong,rem,used,score)
        agn = input("Would you like to play again? (y/n)".center(100)+"\n>> ")
        if agn.lower() == 'y':
            continue
        else:
            if score > int(scoreArr[2][1]):
                print("\n\n\nYOU GOT A HIGH SCORE!!\nWelcome to the Committee of Public Safety\n\n",center(100))
                name = input("Enter your party name, Comrade:  ".center(100))
                if score > int(scoreArr[1][1]):
                    if score > int(scoreArr[0][1]):
                        scoreArr[0][0] = name
                        scoreArr[0][1] = str(score)
                    else:
                        scoreArr[1][0] = name
                        scoreArr[1][1] = str(score)
                else:
                    scoreArr[2][0] = name
                    scoreArr[2][1] = str(score)
                scrFile = open('highScores',w)
                for s in range(len(scoreArr)):
                    scoreArr[s] = " ".join(scoreArr[s])
                wrt = "\n".join(scoreArr)
                scrFile.write(wrt)
            print("\n\nThanks for playing. Workers have nothing to lose but their chains!!")
            exit()

def getWord():
    picker = open('wordlist')
    lst = picker.read().split("\n")
    picker.close()
    print(lst)
    word = lst[round(random.random()*(len(lst)-1))].upper()
    return word

def game(wd,rv,wr,rm,us,scr):
    msg = ""
    while True:
        printStatus(rm,us,wr,rv,msg)
        g = guess(rm,us,wr,wd,rv)
        if g in wd:
            msg = "Good Guess!"
            for i in range(len(wd)):
                if g == wd[i]:
                    rv[i] = wd[i]
        else:
            msg = "Sorry, that's wrong..."
            wr +=1
        us.append(g)
        if isWin(us,wd):
            winner(wd,us,scr)
            break
        elif isLose(wr):
            loser()
            break

def printStatus(rem, us, wr, wd,msg):
    for r in range(40):
        print()
    letLine = 'Letters: [ '
    for a in range(len(rem)):
        if rem[a] in us:
            letLine = letLine + "_"
        else:
            letLine = letLine + rem[a]
        if a<len(rem)-1:
            letLine = letLine + ", "
        else:
            letLine = letLine + "]\n"
    print(letLine.center(100))
    print("\n")
    useLine = "Used:  "
    useStr = ", ".join(us)
    useLine = useLine + useStr
    print(useLine.center(100))
    print()
    print()
    print(("WRONG: " + str(wr) +"\n\n").center(100))
    print(("Turns Left: " + str(10-wr)+"\n\n").center(100))
    print("\n")
    print((" ".join(wd)).center(100))
    print(("\n\n"+msg+"\n\n").center(100))

def guess(rem, us, wr, wd, rv):
    while True:
        gues = input("Guess a Letter:  ").upper()
        if gues in us:
            print("\nAlready Tried that one\n")
            continue
        elif gues == "QUIT":
            exit()
        elif gues not in rem:
            print('Uhh... That\'s not a letter')
            continue
        else:
            return gues

def isWin(use,wrd):
    for w in wrd:
        if w not in use and w != ' ':
            return False
    return True

def isLose(wrn):
    if wrn > 9:
        return True
    else:
        return False

def winner(wrd,use,scr):
    print("\n\n\n")
    score = 10 + len(wrd) - len(use)
    scr += score
    print(' '.join(wrd).center(100))
    print("\n\n\n")
    print(("Congrats. Another bourgeois can meet the people's barber.").center(100))
    print("\n")
    print(("You Scored: " + str(score)).center(100))
    print("\n")
    print(("Your Total Score is: " +str(scr)).center(100))

def loser():
    print("\n\n\n")
    print("Sorry, Comrade! Better luck next time\n\n")



main()
